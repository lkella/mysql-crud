var mysql = require("mysql");

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root123",
  database: "Firstdb",
});

con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");

  //To execute query
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("query executed!");
  });

  //   To view output in console
  if (x === 3) {
    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log(result);
    });
  } else if (x === 10) {
    console.log("Table deleted successfully");
  } else if (x === 11) {
    console.log("Database deleted successfully");
  } else {
    var readTable = "select * from employees";
    con.query(readTable, function (err, result) {
      if (err) throw err;
      console.log(result);
    });
  }
});

// Function to create a database
const createDb = (dbname) => {
  return `CREATE DATABASE ${dbname}`;
};

// Function to specify database to be used
const useDb = (dbname) => {
  return `use ${dbname}`;
};

// Function to create a table
const createTable = (tablename, col1, col2, col3, col4) => {
  return `CREATE TABLE ${tablename} (${col1} VARCHAR(50),${col2} int(10), ${col3} int(10), ${col4} int(10))`;
};

// Function to read the metadata of the table
const readTable = (tablename) => {
  return `DESC ${tablename}`;
};

// Function to add primarykey for a column
const addPrimaryKey = (tablename, constraint, colName) => {
  return `ALTER TABLE ${tablename} ADD CONSTRAINT ${constraint} PRIMARY KEY (${colName})`;
};

// Function to insert records
const insertRecords = (
  tablename,
  col1,
  col2,
  col3,
  col4,
  value1,
  value2,
  value3,
  value4
) => {
  return `INSERT INTO ${tablename} (${col1},${col2},${col3},${col4}) VALUES (${value1},${value2},${value3},${value4})`;
};

// Function to read records
const readRecords = (tablename) => {
  return `SELECT * FROM ${tablename}`;
};

// Function to update records
const updateRecords = (tablename, updateCol, updateValue, keyCol, keyValue) => {
  return `UPDATE ${tablename} SET ${updateCol} = ${updateValue} WHERE ${keyCol} = ${keyValue}`;
};

// Function to delete records
const deleteRecords = (tablename, keyCol, keyValue) => {
  return `DELETE FROM ${tablename} WHERE ${keyCol} = ${keyValue}`;
};

// Function to delete Table
const deleteTable = (tablename) => {
  return `DROP TABLE ${tablename}`;
};

// Function to delete database
const deleteDatabase = (dbname) => {
  return `DROP DATABASE ${dbname}`;
};

var sql = "";
var runQuery = (query) => {
  switch (query) {
    case 1:
      // create database
      sql = createDb("lavanyadb");
      break;
    case 2:
      // Use database
      sql = useDb("lavanyadb");
      break;
    case 3:
      // create table
      sql = createTable(
        "employees",
        "employee_name",
        "employee_id",
        "salary",
        "phone_number"
      );
      break;
    case 4:
      // Read Table
      sql = readTable("employees");
      break;
    case 5:
      // Add primary key
      sql = addPrimaryKey("employees", "Emp_key", "id");
      break;
    case 6:
      // Create records
      sql = insertRecords(
        "employees",
        "employee_name",
        "employee_id",
        "salary",
        "phone_number",
        "'ananya'",
        "'85852'",
        "'48210'",
        "'785221525'"
      );
      break;
    case 7:
      // Read records
      sql = readRecords("employees");
      break;
    case 8:
      // Update records
      sql = updateRecords(
        "employees",
        "employee_name",
        "rupa",
        "employee_id",
        "85852"
      );
      break;
    case 9:
      // Delete records
      sql = deleteRecords("employees", "id", "85852");
      break;
    case 10:
      // Delete table
      sql = deleteTable("employees");
      break;
    case 11:
      // Delete Database
      sql = deleteDatabase("lavanyadb");
      break;
  }
};

var x = 7;
runQuery(x);
